Place link to the addressed issue here. Also it's better to name your branch and MR #issue_id-description.

## What

Describe what you have done in a human readable manner. For a detailed description use [CHANGELOG.md](https://gitlab.com/practic-47/practicqrs/blob/master/CHANGELOG.md).

## Why 

Describe your motivation. Link issues, discussions, etc.

## How

Describe how you did it and why you did it like that. Don't copy paste your commit message, use more free form and try to look at it from maintainers point of view. 

### Testing

Don't forget to describe how to test your PR, step by step. 
