FROM debian:latest
EXPOSE 7001
COPY target/release/practicqrs ./app
COPY .env .
COPY key.pem .
COPY cert.pem .
RUN apt-get update && apt-get -y upgrade && apt-get install -y libpq-dev wait-for-it 
CMD ["./app"]
