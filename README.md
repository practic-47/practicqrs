# PractiCQRS

CQRS based self-invoking microservice.
All functionality communicates via messages ([commands and events](#)),
providing an ability of loosely coupled functions inside microservice
and out of the box integrations with other microservices.

## Functionality

PractiCQRS encapsulates several domains:

- Courses Catalog (Catalog) placed under `/courses` which provides information 
about [courses](#) via a set of `GET` queries and an ability to manage
them via [commands](#). Catalog also sends events about actions
performed with [courses](#).
- Orders Management System (Ordering) placed under `/courses` which provides 
information about [orders](#) via a set of `GET` queries and an ability
to manage them via [commands](#). Ordering also sends events about actions
performed with [orders](#).
- Courses Inventory (Inventory) placed under `/courses` which provides 
information about [instances](#) via a set of `GET` queries and an ability
to manage them via [commands](#). Inventory also sends events about actions
performed with [instances](#).

### Courses Catalog

Catalog placed under `/catalog` which provides information 
about [courses](#) via a set of `GET` queries and an ability to manage
them via [commands](#). Catalog also sends events about actions
performed with [courses](#).

#### Get Available Courses

```bash
curl --request GET --url http://{host}:{port}/catalog/courses/
```

#### Create new Course 

```bash
curl --request PUT --url http://{host}:{port}/catalog/courses/ \
  --header 'content-type: application/json' \
  --data '{
	"icon": "test",
	"picture": "picture",
	"dates": "dates",
	"cost": "cost",
	"agenda": "agenda",
	"title": "title",
	"description": "description"
}'
```

### Orders Management System

Ordering placed under `/ordering` which provides 
information about [orders](#) via a set of `GET` queries and an ability
to manage them via [commands](#). Ordering also sends events about actions
performed with [orders](#).

#### Order a Course

```bash
curl --request PUT --url http://{host}:{port}/ordering/courses/{course_id}/orders/ 
```

#### List all Orders

```bash
curl --request GET --url http://{host}:{port}/ordering/orders/
```

### Courses Inventory 

Inventory placed under `/inventory` which provides 
an ability to manage [instances](#) via `REST` API.

#### Create Instance

```bash
curl --request PUT --url http://{host}:{port}/inventory/instances/ \
  --header 'content-type: application/json' \
  --data '{
	"recepient": "recepient",
	"specification": "specification"
}'
```

## Stories

### Student buys a Course

```
  Given Student is authenticated in the system
    And at least one Course is available
   When Student buys this Course
   Then Order in `PENDING` state created  
    And Payment Request sent 
    And Notification to Sales Managers sent
```

### Administrator looks Orders 

```
    Given Administrator is authenticated in the system
      And 1000 Orders are available 
     When Administrator looks for Orders
     Then 100 latest Orders presented
      And the link to the next 100 Orders presented
```

### Student looks Courses 

```
    Given Student is authenticated in the system
      And 1000 Courses are available 
     When Student looks for Courses
     Then 100 latest Courses presented
      And the link to the next 100 Courses presented
```
