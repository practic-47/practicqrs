Feature: Catalog Feature

  Scenario: Student looks Courses 
    Given Student is authenticated in the system
      And 1000 Courses are available 
     When Student looks for Courses
     Then 100 latest Courses presented
      And the link to the next 100 Courses presented
