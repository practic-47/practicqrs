  Feature: Courses Ordering feature
  
  Scenario: Administrator looks Orders 
    Given Administrator is authenticated in the system
      And 1000 Orders are available 
     When Administrator looks for Orders
     Then 100 latest Orders presented
      And the link to the next 100 Orders presented

  Scenario: Student buys a Course
    Given Student is authenticated in the system
      And at least one Course is available
     When Student buys this Course
     Then Order in `Created` state created  
      And Payment Request sent 
      And Notification to Sales Managers sent
