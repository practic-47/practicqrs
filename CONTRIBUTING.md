# Contributing Guide

## Development

### No `unwrap`

Use `.expect("Good error message to be printed in stdout (one sentence starting with Capital letter and ending with dot)."`.

### Worth knowing

PractiCQRS actively uses `actix` and other tools based on `futures` concept, so it's better to check these resources:
- https://tokio.rs/docs/getting-started/
- https://docs.rs/futures/0.3.1/futures/

## Documentation

Before writing documentation please read:

- [Rust Book - Making Useful Documentation Comments](https://doc.rust-lang.org/stable/book/ch14-02-publishing-to-crates-io.html?highlight=docs#making-useful-documentation-comments)

## How To

### Choose Issue

1. Open Issues
1. Find unassigned Issue
1. Assign yourself to this Issue

### Git Workflow

`git pull -r` --> make changes --> `git commit -a -m "One line message."` --> `git push origin master:issue_number_and_short_branch_name`

To fix something in Merge Request use `--amend` flag in your commit.

## Rules

- URLs should end with `/`
- Resources in URLs should be plural

## Known Issues

### Development on ARM

#### Topic not found

```
/opt/kafka_2.12-0.11.0.1/bin# kafka-topics.sh --create --replication-factor 1 --partitions 1 --topic commands --zookeeper zookeeper:2181
```
