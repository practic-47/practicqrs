use crate::{persistence::*, schema::courses};
use actix_web::{web, HttpResponse};
use chrono::offset::Utc;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, std::fmt::Debug, Queryable)]
pub struct Course {
    pub id: i32,
    pub title: String,
    pub description: String,
    pub agenda: Option<String>,
    pub cost: String,
    pub dates: String,
    pub picture: Option<String>,
    pub icon: Option<String>,
}

//TODO: INTO COMMAND
#[derive(Serialize, Deserialize, Insertable, std::fmt::Debug)]
#[table_name = "courses"]
pub struct CreateNewCourseRequest {
    pub icon: String,
    pub picture: String,
    pub dates: String,
    pub cost: String,
    pub agenda: String,
    pub title: String,
    pub description: String,
}

#[derive(Serialize, Deserialize, std::fmt::Debug)]
pub struct CreateNewCourseCommand {
    pub request: CreateNewCourseRequest,
    event: String,
    created: String,
}

impl std::convert::From<&[u8]> for CreateNewCourseCommand {
    fn from(slice: &[u8]) -> Self {
        serde_json::from_slice(slice).expect("Failed to deserialize New Course Command.")
    }
}

impl std::convert::From<CreateNewCourseRequest> for CreateNewCourseCommand {
    fn from(request: CreateNewCourseRequest) -> Self {
        CreateNewCourseCommand {
            request: request,
            event: "CreateNewCourse".to_string(),
            created: Utc::now().to_rfc3339(),
        }
    }
}

pub fn build_courses_catalog_scope() -> actix_web::Scope {
    web::scope("/catalog")
        .route(
            "/courses/",
            web::get().to(|data: web::Data<Environment>| {
                use crate::schema::courses::dsl::*;
                HttpResponse::Ok().json(
                    courses
                        .load::<Course>(&data.connection)
                        .expect("There is problem with loading courses"),
                )
            }),
        )
        .route(
            "/courses/{course_id}/",
            web::get().to(|path: web::Path<i32>, data: web::Data<Environment>| {
                use crate::schema::courses::dsl::*;
                HttpResponse::Ok().json(
                    courses
                        .find(path.into_inner())
                        .first::<Course>(&data.connection)
                        .expect("Failed to find a course."),
                )
            }),
        )
        .route(
            "/courses/",
            web::put().to(
                |request: web::Json<CreateNewCourseRequest>, data: web::Data<Environment>| {
                    use crate::schema::courses::dsl::*;
                    use practic_events_lib::TelegramNotification;
                    let created_course = diesel::insert_into(courses)
                        .values(&request.into_inner())
                        .get_result::<Course>(&data.connection)
                        .expect("Error saving a new course.");
                    //TODO: why we always use "".to_string here?
                    infrastructure::Messenger::new().publish(
                        "notifications".to_string(),
                        serde_json::to_vec(&TelegramNotification {
                            chat_id: 4103090,
                            message: format!("Course {} created.", &created_course.title),
                        })
                        .expect("Failed to serialize notification."),
                    );
                    HttpResponse::Ok().json(created_course)
                },
            ),
        )
        .route(
            "/courses/{course_id}",
            web::delete().to(|path: web::Path<i32>, data: web::Data<Environment>| {
                use crate::schema::courses::dsl::*;
                diesel::delete(courses.filter(id.eq(path.into_inner())))
                    .execute(&data.connection)
                    .expect("Failed to delete course.");
                HttpResponse::NoContent()
            }),
        )
        .route(
            "/courses/",
            web::delete().to(|data: web::Data<Environment>| {
                use crate::schema::courses::dsl::*;
                diesel::delete(courses)
                    .execute(&data.connection)
                    .expect("Failed to delete all courses.");
                HttpResponse::NoContent()
            }),
        )
}

#[cfg(test)]
mod tests {
    use crate::catalog::*;
    use crate::persistence;
    use actix_web::dev::Service;
    use actix_web::{test, web, App};
    use std::sync::Mutex;
    embed_migrations!();

    #[test]
    fn test_get_all_courses_ok() {
        dotenv::from_filename("local.env").ok();
        let connection = persistence::establish_connection();
        let _result = embedded_migrations::run_with_output(&connection, &mut std::io::stdout());
        let environment = web::Data::new(Environment {
            customer_name: String::from("Customer name"),
            messenger: Mutex::new(Option::None),
            connection: connection,
        });
        let mut app = test::init_service(
            App::new()
                .register_data(environment)
                .service(build_courses_catalog_scope()),
        );
        let _json = CreateNewCourseRequest {
            icon: "icon".to_string(),
            picture: "picture".to_string(),
            dates: "dates".to_string(),
            cost: "cost".to_string(),
            agenda: "agenda".to_string(),
            title: "title".to_string(),
            description: "description".to_string(),
        };
        let req = test::TestRequest::get()
            .uri("/catalog/courses/")
            .to_request();
        let resp = test::block_on(app.call(req)).expect("Failed to block on response.");
        assert!(resp.status().is_success());
        //TODO: Assert json fields.
    }

    #[test]
    fn test_create_new_course_ok() {
        dotenv::from_filename("local.env").ok();
        let connection = persistence::establish_connection();
        let _result = embedded_migrations::run_with_output(&connection, &mut std::io::stdout());
        let messenger = infrastructure::Messenger::new();
        let environment = web::Data::new(Environment {
            customer_name: String::from("Customer name"),
            messenger: Mutex::new(Option::Some(messenger)),
            connection: persistence::establish_connection(),
        });
        let mut app = test::init_service(
            App::new()
                .register_data(environment)
                .service(build_courses_catalog_scope()),
        );
        let json = CreateNewCourseRequest {
            icon: "icon".to_string(),
            picture: "picture".to_string(),
            dates: "dates".to_string(),
            cost: "cost".to_string(),
            agenda: "agenda".to_string(),
            title: "title".to_string(),
            description: "description".to_string(),
        };
        let req = test::TestRequest::put()
            .uri("/catalog/courses/")
            .set_json(&json)
            .to_request();
        let resp = test::block_on(app.call(req)).expect("Failed to block on response.");
        println!("Response Status is {:?}", resp.status());
        assert!(resp.status().is_success());
    }

    #[test]
    fn test_get_course_by_id_ok() {
        dotenv::from_filename("local.env").ok();
        let connection = persistence::establish_connection();
        let _result = embedded_migrations::run_with_output(&connection, &mut std::io::stdout());
        let messenger = infrastructure::Messenger::new();
        let environment = web::Data::new(Environment {
            customer_name: String::from("Customer name"),
            messenger: Mutex::new(Option::Some(messenger)),
            connection: connection,
        });
        let mut app = test::init_service(
            App::new()
                .register_data(environment)
                .service(build_courses_catalog_scope()),
        );
        let json = CreateNewCourseRequest {
            icon: "icon".to_string(),
            picture: "picture".to_string(),
            dates: "dates".to_string(),
            cost: "cost".to_string(),
            agenda: "agenda".to_string(),
            title: "title".to_string(),
            description: "description".to_string(),
        };
        let req_to_create = test::TestRequest::put()
            .uri("/catalog/courses/")
            .set_json(&json)
            .to_request();
        let course_to_get: Course = test::read_response_json(&mut app, req_to_create);
        let req_to_get = test::TestRequest::delete()
            .uri(&format!("/catalog/courses/{}", course_to_get.id))
            .to_request();
        let response = test::call_service(&mut app, req_to_get);
        println!("Response Status is {:?}", response.status());
        assert!(response.status().is_success());
    }

    #[test]
    fn test_delete_course_no_content() {
        dotenv::from_filename("local.env").ok();
        let connection = persistence::establish_connection();
        let _result = embedded_migrations::run_with_output(&connection, &mut std::io::stdout());
        let messenger = infrastructure::Messenger::new();
        let environment = web::Data::new(Environment {
            customer_name: String::from("Customer name"),
            messenger: Mutex::new(Option::Some(messenger)),
            connection: persistence::establish_connection(),
        });
        let mut app = test::init_service(
            App::new()
                .register_data(environment)
                .service(build_courses_catalog_scope()),
        );
        let json = CreateNewCourseRequest {
            icon: "icon".to_string(),
            picture: "picture".to_string(),
            dates: "dates".to_string(),
            cost: "cost".to_string(),
            agenda: "agenda".to_string(),
            title: "title".to_string(),
            description: "description".to_string(),
        };
        let req_to_create = test::TestRequest::put()
            .uri("/catalog/courses/")
            .set_json(&json)
            .to_request();
        let course_to_delete: Course = test::read_response_json(&mut app, req_to_create);
        let req_to_delete = test::TestRequest::delete()
            .uri(&format!("/catalog/courses/{}", course_to_delete.id))
            .to_request();
        let response = test::call_service(&mut app, req_to_delete);
        println!("Response Status is {:?}", response.status());
        assert!(response.status().is_success());
    }

    #[test]
    fn test_delete_all_courses_no_content() {
        dotenv::from_filename("local.env").ok();
        let connection = persistence::establish_connection();
        let _result = embedded_migrations::run_with_output(&connection, &mut std::io::stdout());
        let messenger = infrastructure::Messenger::new();
        let environment = web::Data::new(Environment {
            customer_name: String::from("Customer name"),
            messenger: Mutex::new(Option::Some(messenger)),
            connection: persistence::establish_connection(),
        });
        let mut app = test::init_service(
            App::new()
                .register_data(environment)
                .service(build_courses_catalog_scope()),
        );
        let json = CreateNewCourseRequest {
            icon: "icon".to_string(),
            picture: "picture".to_string(),
            dates: "dates".to_string(),
            cost: "cost".to_string(),
            agenda: "agenda".to_string(),
            title: "title".to_string(),
            description: "description".to_string(),
        };
        let req_to_create = test::TestRequest::put()
            .uri("/catalog/courses/")
            .set_json(&json)
            .to_request();
        test::call_service(&mut app, req_to_create);
        let req_to_delete = test::TestRequest::delete()
            .uri("/catalog/courses/")
            .to_request();
        let response = test::call_service(&mut app, req_to_delete);
        println!("Response Status is {:?}", response.status());
        assert!(response.status().is_success());
    }
}
