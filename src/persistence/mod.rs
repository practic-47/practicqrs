use diesel::pg::PgConnection;
use diesel::prelude::*;
use std::sync::Mutex;

/// Environment represents application state shared between different actors.
/// Some fields like `customer_name` represent customer specifics.
/// Mostly consists of technical dependencies like connections, producers, etc.
pub struct Environment {
    pub customer_name: String,
    pub messenger: Mutex<Option<infrastructure::Messenger>>,
    pub connection: PgConnection,
}

pub fn establish_connection() -> PgConnection {
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url))
}

pub fn create_ssl_builder() -> openssl::ssl::SslAcceptorBuilder {
    use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};
    let mut builder =
        SslAcceptor::mozilla_intermediate(SslMethod::tls()).expect("Failed to create SSL Buider.");
    builder
        .set_private_key_file("key.pem", SslFiletype::PEM)
        .expect("Failed to set Private Key.");
    builder
        .set_certificate_chain_file("cert.pem")
        .expect("Failed to set certificate.");
    builder
}
