use crate::persistence::Environment;
use crate::schema::instances;
use actix_web::web;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Insertable)]
#[table_name = "instances"]
struct CreateInstanceRequest {
    recepient: String,
    specification: String,
}

#[derive(Queryable, Serialize)]
struct Instance {
    pub id: i32,
    pub recepient: String,
    pub specification: String,
}

pub fn build_inventory_scope() -> actix_web::Scope {
    web::scope("/inventory/instances").route(
        "/",
        web::put().to(
            |request: web::Json<CreateInstanceRequest>, data: web::Data<Environment>| {
                use crate::schema::instances::dsl::*;
                web::Json(
                    diesel::insert_into(instances)
                        .values(&request.into_inner())
                        .get_result::<Instance>(&data.connection)
                        .expect("Error saving new instance"),
                )
            },
        ),
    )
}
