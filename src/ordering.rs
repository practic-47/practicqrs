use crate::persistence::*;
use crate::schema::orders;
use actix_web::web;
use diesel::prelude::*;
use std::{
    convert::{TryFrom, TryInto},
    time::SystemTime,
};

#[derive(serde::Serialize, serde::Deserialize, Copy, Clone, Debug, DbEnum, PartialEq)]
#[PgType = "order_status"]
#[DieselType = "Order_status"]
pub enum OrderStatus {
    Created,
    Processing,
    Completed,
}

#[derive(Identifiable, Queryable, serde::Serialize, serde::Deserialize, Clone, Debug)]
pub struct Order {
    pub id: i32,
    pub specification: String,
    pub status: OrderStatus,
}

#[derive(Insertable, serde::Deserialize)]
#[table_name = "orders"]
pub struct NewOrder {
    pub specification: String,
    pub status: OrderStatus,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct OrderCreated {
    pub order: Order,
    pub timestamp: SystemTime,
    //TODO: add principal info
}

impl From<Order> for OrderCreated {
    fn from(order: Order) -> Self {
        OrderCreated {
            order: order,
            timestamp: SystemTime::now(),
        }
    }
}

impl TryFrom<Vec<u8>> for OrderCreated {
    type Error = serde_json::Error;

    fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
        serde_json::from_slice(&value)
    }
}

pub fn build_ordering_scope() -> actix_web::Scope {
    web::scope("/ordering")
        .route(
            "/orders/",
            web::get().to(|data: web::Data<Environment>| {
                use crate::schema::orders::dsl::*;
                web::Json(
                    orders
                        .load::<Order>(&data.connection)
                        .expect("Error loading orders."),
                )
            }),
        )
        .route(
            "/courses/{course_id}/orders/",
            web::put().to(|path: web::Path<String>, data: web::Data<Environment>| {
                use crate::schema::orders::dsl::*;
                let order = diesel::insert_into(orders)
                    .values(NewOrder {
                        specification: path.into_inner(),
                        status: OrderStatus::Created,
                    })
                    .get_result::<Order>(&data.connection)
                    .expect("Error saving new order.");
                &data
                    .messenger
                    .lock()
                    .expect("Failed to lock Messenger.")
                    .as_mut()
                    .expect("Producer is not provided.")
                    .publish(
                        "events".to_string(),
                        serde_json::to_vec(&OrderCreated::from(order.clone()))
                            .expect("Failed to serialize Order Created Event."),
                    );
                web::Json(order)
            }),
        )
}

pub fn receive_command(message: Vec<u8>) {
    println!("Event received: {:?}", message);
    let event = OrderCreated::try_from(message).expect("Failed to deserialize event.");
    let messenger = infrastructure::Messenger::new();
    messenger.publish(
        "notifications".to_string(),
        practic_events_lib::TelegramNotification {
            chat_id: 4103090,
            message: format!("{} order created", event.order.specification),
        }
        .try_into()
        .expect("Failed to serialize notification."),
    );
    use crate::schema::orders::dsl::*;
    diesel::update(&event.order)
        .set(status.eq(OrderStatus::Processing))
        .execute(&crate::persistence::establish_connection())
        .expect("Failed to update orders status.");
    use isahc::prelude::*;
    let mut response = Request::put(format!(
        "{}/accounts/1/send/2",
        std::env::var("PAYMENTS_URI").expect("PAYMENTS_URI env should be set.")
    ))
    .header("Content-Type", "application/json")
    .body(
        r#"{
            "amount": 300,
            "currency": "RUB"
        }"#,
    )
    .expect("Failed to build payment request.")
    .send()
    .expect("Failed to send payment request.");
    println!("{:?} response.", response);
    let payslip = response
        .json::<Payslip>()
        .expect("Failed to deserialize payslip.");
    println!("{:?} payslip.", payslip);
}

#[derive(serde::Deserialize, Debug)]
struct Payslip {
    sender_id: u32,
    receiver_id: u32,
    amount: u32,
    currency: String,
}

#[cfg(test)]
mod tests {
    use crate::ordering::*;
    use crate::persistence;
    use actix_web::dev::Service;
    use actix_web::{test, web, App};
    use std::sync::Mutex;
    embed_migrations!();

    #[test]
    fn test_get_all_orders_ok() {
        dotenv::from_filename("local.env").ok();
        let connection = persistence::establish_connection();
        let _result = embedded_migrations::run_with_output(&connection, &mut std::io::stdout());
        let environment = web::Data::new(Environment {
            customer_name: String::from("Customer name"),
            messenger: Mutex::new(Option::None),
            connection: connection,
        });
        let mut app = test::init_service(
            App::new()
                .register_data(environment)
                .service(build_ordering_scope()),
        );
        let req = test::TestRequest::get()
            .uri("/ordering/orders/")
            .to_request();
        let resp = test::block_on(app.call(req)).expect("Failed to block on response.");
        assert!(resp.status().is_success());
    }

    #[test]
    fn test_create_new_order_ok() {
        dotenv::from_filename("local.env").ok();
        let messenger = infrastructure::Messenger::new();
        let environment = web::Data::new(Environment {
            customer_name: String::from("Customer name"),
            messenger: Mutex::new(Option::Some(messenger)),
            connection: persistence::establish_connection(),
        });
        let mut app = test::init_service(
            App::new()
                .register_data(environment)
                .service(build_ordering_scope()),
        );
        let req = test::TestRequest::put()
            .uri("/ordering/courses/1/orders/")
            .to_request();
        let created_order: Order = test::read_response_json(&mut app, req);
        println!("Created order: {:?}", created_order);
        assert_eq!(created_order.specification, "1");
        assert_eq!(created_order.status, OrderStatus::Created);
    }

    #[test]
    fn test_create_new_order_ok_and_payment_sent() {
        dotenv::from_filename("local.env").ok();
        let messenger = infrastructure::Messenger::new();
        messenger.subscribe("events".to_string(), receive_command);
        let environment = web::Data::new(Environment {
            customer_name: String::from("Customer name"),
            messenger: Mutex::new(Option::Some(messenger)),
            connection: persistence::establish_connection(),
        });
        let mut app = test::init_service(
            App::new()
                .register_data(environment)
                .service(build_ordering_scope()),
        );
        let req = test::TestRequest::put()
            .uri("/ordering/courses/1/orders/")
            .to_request();
        let created_order: Order = test::read_response_json(&mut app, req);
        println!("Created order: {:?}", created_order);
        assert_eq!(created_order.specification, "1");
        assert_eq!(created_order.status, OrderStatus::Created);
        use std::{thread, time};
        thread::sleep(time::Duration::from_millis(200));
    }
}
