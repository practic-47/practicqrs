extern crate chrono;
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
extern crate dotenv;
extern crate tokio;

use diesel_migrations::*;
use practicqrs::*;

embed_migrations!();

/// Main entry point in PractiCQRS application.
/// During execution it:
/// - loads environment variables from a .env file;
/// - establishes connection with a database;
/// - runs migration scripts in the database;
/// - spawns an additional thread for an HTTP Server and a Commands Producer;
/// - consumes Commands in the main thread;
fn main() {
    use actix_cors::Cors;
    use actix_web::{middleware::Logger, web, App, HttpServer};
    dotenv::dotenv().ok();
    env_logger::init();
    let connection = persistence::establish_connection();
    embedded_migrations::run_with_output(&connection, &mut std::io::stdout())
        .expect("Database migration failed");
    let service_url = std::env::var("SERVICE_URL").unwrap_or("0.0.0.0:7001".to_string());
    let _address = HttpServer::new(move || {
        let messenger = infrastructure::Messenger::new();
        messenger.subscribe("events".to_string(), ordering::receive_command);
        App::new()
            .register_data(web::Data::new(persistence::Environment {
                customer_name: String::from("Customer name"),
                messenger: std::sync::Mutex::new(Option::Some(messenger)), //TODO: remove Option from struct definition
                connection: persistence::establish_connection(),
            }))
            .wrap(Cors::default())
            .wrap(Logger::default())
            .service(catalog::build_courses_catalog_scope())
            .service(ordering::build_ordering_scope())
            .service(inventory::build_inventory_scope())
    })
    .bind_ssl(service_url, persistence::create_ssl_builder())
    .expect("Failed to bind Http Server.")
    .run();
}
