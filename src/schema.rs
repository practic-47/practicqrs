table! {
    use diesel::sql_types::*;
    use crate::ordering::Order_status;

    courses (id) {
        id -> Int4,
        title -> Varchar,
        description -> Text,
        agenda -> Nullable<Text>,
        cost -> Text,
        dates -> Text,
        picture -> Nullable<Varchar>,
        icon -> Nullable<Varchar>,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::ordering::Order_status;

    instances (id) {
        id -> Int4,
        recepient -> Varchar,
        specification -> Varchar,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::ordering::Order_status;

    orders (id) {
        id -> Int4,
        specification -> Varchar,
        status -> Order_status,
    }
}

allow_tables_to_appear_in_same_query!(courses, instances, orders,);
