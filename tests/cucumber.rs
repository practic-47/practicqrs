use crate::persistence;
use actix_web::{web, App};
use cucumber::cucumber;
use diesel::pg::PgConnection;
use practicqrs::*;
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

embed_migrations!();

pub struct TestApp {
    user: i32,
    connection: PgConnection,
    address: String,
    response: Option<isahc::prelude::Response<isahc::Body>>,
}

impl cucumber::World for TestApp {}

impl std::default::Default for TestApp {
    fn default() -> TestApp {
        TestApp {
            user: 0,
            connection: persistence::establish_connection(),
            address: "http://0.0.0.0:7001".to_string(),
            response: Option::None,
        }
    }
}

mod orders_steps {
    use crate::catalog;
    use crate::ordering::*;
    use cucumber::steps;
    use diesel::prelude::*;
    use isahc::prelude::*;

    steps!(crate::TestApp => {
        given "Administrator is authenticated in the system" |world, _step| {
            world.user = 1;
        };

        given "Student is authenticated in the system" |world, _step| {
            world.user = 2;
        };

        given "1000 Orders are available" |world, _step| {
            use crate::schema::orders::dsl::*;
            let mut orders_to_insert = vec![
                NewOrder {
                    specification: "0".to_string(),
                    status: OrderStatus::Created,
                }
            ];
            for i in 0..998 {
                orders_to_insert.push(NewOrder {
                    specification: i.to_string(),
                    status: OrderStatus::Created,
                });
            }
            diesel::insert_into(orders)
                .values(orders_to_insert)
                .get_result::<Order>(&world.connection)
                .expect("Error saving orders.");
        };

        given "1000 Courses are available" |world, _step| {
            use crate::schema::courses::dsl::*;
            let mut entities_to_insert = vec![
                catalog::CreateNewCourseRequest {
                        icon: "icon".to_string(),
                        picture: "picture".to_string(),
                        dates: "dates".to_string(),
                        cost: "cost".to_string(),
                        agenda: "agenda".to_string(),
                        title: "0".to_string(),
                        description: "description".to_string(),
                    }
            ];
            for i in 0..998 {
                entities_to_insert.push(
                    catalog::CreateNewCourseRequest {
                        icon: "icon".to_string(),
                        picture: "picture".to_string(),
                        dates: "dates".to_string(),
                        cost: "cost".to_string(),
                        agenda: "agenda".to_string(),
                        title: i.to_string(),
                        description: "description".to_string(),
                    }
                );
            }
            diesel::insert_into(courses)
                .values(entities_to_insert)
                .get_result::<catalog::Course>(&world.connection)
                .expect("Error saving courses.");
        };

        given "at least one Course is available" |world, _| {
            use crate::schema::courses::dsl::*;
            diesel::insert_into(courses)
                .values(
                    catalog::CreateNewCourseRequest {
                        icon: "icon".to_string(),
                        picture: "picture".to_string(),
                        dates: "dates".to_string(),
                        cost: "cost".to_string(),
                        agenda: "agenda".to_string(),
                        title: "title".to_string(),
                        description: "description".to_string(),
                    }
                )
                    .get_result::<catalog::Course>(&world.connection)
                    .expect("Error saving a new course.");
        };

        when "Administrator looks for Orders" |world,_| {
            world.response = Option::Some(
                isahc::get(format!("{}/ordering/orders/", world.address)).expect("Failed to retrieve orders.")
            );
            println!("Response {:?}", world.response);
        };

        when "Student looks for Courses" |world,_| {
            world.response = Option::Some(
                isahc::get(format!("{}/catalog/courses/", world.address)).expect("Failed to retrieve courses.")
            );
            println!("Response {:?}", world.response);
        };

        when "Student buys this Course" |world,_| {
            world.response = Option::Some(
                isahc::put(format!("{}/ordering/courses/1/orders/", world.address), ()).expect("Failed to create order.")
            );
            println!("Response {:?}", world.response);
        };

        then "100 latest Orders presented" |world,_| {
            println!("Response {:?}", world.response);
            assert!(world.response.as_ref().unwrap().status().is_success());
        };

        then "100 latest Courses presented" |world,_| {
            println!("Response {:?}", world.response);
            assert!(world.response.as_ref().unwrap().status().is_success());
        };

        then "the link to the next 100 Courses presented" |world,_| {
            println!("Response {:?}", world.response);
            assert!(world.response.as_ref().unwrap().status().is_success());
        };

        then "the link to the next 100 Orders presented" |world,_| {
            println!("Response {:?}", world.response);
            assert!(world.response.as_ref().unwrap().status().is_success());
        };

        then "Order in `Created` state created" |world,_| {
            println!("Response {:?}", world.response);
            let response = world.response.as_mut().expect("Response is missing.");
            assert!(response.status().is_success());
            let order = response.json::<Order>().expect("Failed to deserialize order.");
            assert_eq!(order.status, OrderStatus::Created);
        };

        then "Payment Request sent" |world,_| {
            println!("Response {:?}", world.response);
            assert!(world.response.as_ref().unwrap().status().is_success());
        };

        then "Notification to Sales Managers sent" |world,_| {
            println!("Response {:?}", world.response);
            assert!(world.response.as_ref().unwrap().status().is_success());
        };
    });
}

fn setup() {
    dotenv::from_filename("local.env").ok();
    let connection = persistence::establish_connection();
    let _result = embedded_migrations::run_with_output(&connection, &mut std::io::stdout());
    std::thread::spawn(|| {
        use actix_cors::Cors;
        use actix_web::{middleware::Logger, HttpServer};
        println!("TestApp creation.");
        HttpServer::new(move || {
            let messenger = infrastructure::Messenger::new();
            messenger.subscribe("events".to_string(), ordering::receive_command);
            App::new()
                .register_data(web::Data::new(persistence::Environment {
                    customer_name: String::from("Customer name"),
                    messenger: std::sync::Mutex::new(Option::Some(messenger)), //TODO: remove Option from struct definition
                    connection: persistence::establish_connection(),
                }))
                .wrap(Cors::default())
                .wrap(Logger::default())
                .service(catalog::build_courses_catalog_scope())
                .service(ordering::build_ordering_scope())
                .service(inventory::build_inventory_scope())
        })
        .bind("0.0.0.0:7001".to_string())
        .expect("Failed to bind Http Server.")
        .run()
        .expect("Failde to run service.");
    });
}

cucumber! {
    features: "./features",
    world: crate::TestApp,
    steps: &[orders_steps::steps],
    setup: setup
}
