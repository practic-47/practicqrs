CREATE TYPE order_status AS ENUM ('created', 'processing', 'completed');
CREATE TABLE orders (
	id SERIAL PRIMARY KEY,
	specification VARCHAR NOT NULL,
        status order_status NOT NULL
)
