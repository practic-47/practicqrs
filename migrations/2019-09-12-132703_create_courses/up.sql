-- Your SQL goes here
CREATE TABLE courses (
	id SERIAL PRIMARY KEY,
	title VARCHAR NOT NULL,
	description TEXT NOT NULL,
	agenda TEXT,
	cost TEXT NOT NULL,
	dates TEXT NOT NULL,
	picture VARCHAR,
	icon VARCHAR
)
