# Changelog

## [2020-02-14] - 0.1.2

### Added

- `OrderCreated` events' subscription which sends notification and *simulates* processing.
- `Status` attribute for `Orders`.

## [2020-02-09] - 0.1.1

### Added

- `From` trait impls to reduce boilerplate.
- Delete API for `Courses` domain.

### Changed

- Instead of NATS usage during `Course` creation direct write to the database has been placed.

### Removed

- Additional thread for Http Server.
- Additional `actix` system.

## 0.1.0

### Added

- CI configuration
- Executable rust project structure
- Dependencies for actix-web
- Executable web application
- Basic integration testing
- Dependency on ratsio (NATS Rust client library)
- Basic diesel setup for PostgreSQL
- Migration script and model for courses, orders and instances
- Diesel migration enabled on service startup
- PostgreSQL service added to gitlab CI configuration
- Various Docker Compose configurations 
- Additional thread for HttpServer and for Messanger
- CreateNewCourseCommand constructor with Request as parameter and into_json_string method
- Working `GET` Courses function (REST)
- Working `PUT` new Course function (REST + Messanger)
- Modules for domains and additional module for insfrastructure related functionality.
